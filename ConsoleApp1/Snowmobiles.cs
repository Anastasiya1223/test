﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
        public abstract class Snowmobiles
        {
            #region fields
            /// номер снегохода
            private int number_snowmobil;
            /// Вид
            private string vid;
            /// год выпуска
            private int year;
            /// масса
            private int mass;
            /// номер гаража
            private int number;
            #endregion

            #region Properties
            public int Number_snowmobil
            {
                get
                {
                    return number_snowmobil;
                }
                set
                {
                    number_snowmobil = value;
                }
            }
            /// <summary>
            /// Вид
            /// </summary>
            public string Vid
            {
                get
                {
                    return vid;
                }
                set
                {
                    if (value.Length != 0)
                    {
                        vid = value;
                    }
                    else
                    {
                        vid = "turist";
                        Console.WriteLine("Неудачная попытка установки значения {0} для снегохода {1}. Установлено значение по умолчанию (turist).", value, number_snowmobil);
                    }
                }
            }
            /// год выпуска
            public int Year
            {
                get
                {
                    return year;
                }
                set
                {
                    if (value >= 1980 && value < 2021)
                        year = value;
                    else
                    {
                        year = 2000;
                        Console.WriteLine("Неудачная попытка установки значения {0} год для снегохода {1}. Установлено значение по умолчанию (2000 год).", value, number_snowmobil);
                    }
                }
            }



            /// Масса
            public int Mass
            {
                get
                {
                    return mass;
                }
                set
                {
                    if (value >= 150 && value < 450)
                        mass = value;
                    else
                    {
                        mass = 200;
                        Console.WriteLine(" значения {0} кг для cнегохода {1}. Установлено значение по умолчанию (200 кг).", value, number_snowmobil);
                    }
                }
            }
            /// Номер гаража
            public int Number
            {
                get
                {
                    return number;
                }
                set
                {
                    if (value >= 0 && value < 1000)
                        number = value;
                    else
                    {
                        number = 1;
                        Console.WriteLine("Неудачная попытка установки значения {0} для снегохода{1}. Установлено значение по умолчанию (гараж №1).", value, number_snowmobil);
                    }
                }
            }
            #endregion

            #region Methods

            public virtual void Print()
            {
                Console.WriteLine("Информация о снегоходе:");
                Console.WriteLine("Номер снегохода: {0}", number_snowmobil);
                Console.WriteLine("Вид: {0}", vid);
                Console.WriteLine("Год выпуска: {0}", year);
                Console.WriteLine("Масса: {0}", mass);
                Console.WriteLine("Номер гаража: {0}", number);
                Console.WriteLine();
                Console.ReadKey();

            }

            public void GoToNewGarage()
            {
                Console.WriteLine("Введите новый номер гаража для {0}", number_snowmobil);
                int oldnumber = number;
                Number = int.Parse(Console.ReadLine());
                Console.WriteLine("Снегоход {0} успешно перевезён из {1} в {2} гараж.", number_snowmobil, oldnumber, number);
            }
            #endregion

            #region Constructors
            /// <summary>
            /// генерация случайного числа
            /// </summary>
            /// <returns></returns>
            int NewNumber()
            {
                Random rnd = new Random();
                return rnd.Next(1, 999999);
            }
            public Snowmobiles()
            {
                Number_snowmobil = NewNumber();
                Vid = "-";
                Year = 0;
                Mass = 300;
                Number = 1;
            }
            /// создание экземпляра класса "снегоход"
            /// </summary>
            /// <param name="nm">номер</param>
            /// <param name="vd">вид</param>
            /// <param name="s">год выпуска</param>
            /// <param name="ag">масса</param>
            /// <param name="num">номер гаража</param>
            /// 
            public Snowmobiles(int nm, string vd, int yea, int mas, int num)
            {
                number_snowmobil = nm;
                Vid = vd;
                Year = yea;
                Mass = mas;
                Number = num;
            }

            /// добавление cнегохода по номеру и году выпуска
            /// <param name="nm">имя</param>
            /// <param name="yea">возраст</param>
            public Snowmobiles(int nm, int yea)
            : this(nm, "-", yea, 300, 1) { }

            /// добавление cнегохода только по номеру
            /// <param name="nm"></param>
            public Snowmobiles(int nm)
                : this(nm, "-", 1980, 300, 1) { }
            #endregion

            #region Operators
            
            public static bool operator <(Snowmobiles a, Snowmobiles b)
            {
                return a.mass < b.mass;
            }

            public static bool operator >(Snowmobiles a, Snowmobiles b)
            {
                return a.mass > b.mass;
            }
           
            public static bool operator ==(Snowmobiles a, Snowmobiles b)
            {
                return a.number == b.number && a.mass == b.mass;
            }

            public static bool operator !=(Snowmobiles a, Snowmobiles b)
            {
                return a.number != b.number || a.mass != b.mass;
            }
            #endregion
        }
    }