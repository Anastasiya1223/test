﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Snowmobiles> snowmobiles1 = new List<Snowmobiles>();
            

            using (StreamReader r = new StreamReader("1.txt"))
            {
                string st = r.ReadLine();
                while (st != null)
                {
                    var mas = st.Split(' ');
                    switch (mas[0])
                    {
                        case "Onesky":
                            snowmobiles1.Add(new Onesky(int.Parse(mas[1]), mas[2], int.Parse(mas[3]), int.Parse(mas[4]), int.Parse(mas[5]), int.Parse(mas[6])));
                            break;
                        case "Twoskying":
                            snowmobiles1.Add(new Twoskying(int.Parse(mas[1]), mas[2], int.Parse(mas[3]), int.Parse(mas[4]), int.Parse(mas[5]), int.Parse(mas[6])));
                            break;
                        case "Mountain":
                            snowmobiles1.Add(new Mountain(int.Parse(mas[1]), mas[2], int.Parse(mas[3]), int.Parse(mas[4]), int.Parse(mas[5]), int.Parse(mas[6]), int.Parse(mas[7]),mas[8]));
                            break;
                       
                        default:
                            break;
                    }
                    st = r.ReadLine();
                }
                foreach (var item in snowmobiles1)
                {
                    item.Print();
                    Console.WriteLine();
                }
            }

           //Onesky h = new Onesky(2, "sport", 1500, 3, 3, 2);        
           // Mountain m = new Mountain(2, "sport", 2019, 252, 2, 11, 178, "red");
          
            Console.ReadKey();
        }
    }
}
