﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Twoskying : Snowmobiles
    {
        #region Fields
        /// минимальный радиус поворота
        private int minrp;
        #endregion

        #region Propetries
        public int Minrpovorot
        {
            get
            {
                return minrp;
            }
            set
            {
                if (value >= 1 && value < 100)
                    minrp = value;
                else
                {
                    minrp = 10;
                    Console.WriteLine("Неудачная попытка установки значения {0} метров для cнегохода {1}. Установлено значение по умолчанию (10 метров).", value, minrp);
                }
            }
        }
        #endregion

        #region Methods
        public override void Print()
        {
            base.Print();
            Console.WriteLine("Минимальный радиус поворота: {0}", minrp);
        }
        #endregion

        #region Constructors
        public Twoskying(int nm, string vd, int yea, int mas, int num, int minrp) : base(nm, vd, yea, mas, num)
        {
            Minrpovorot = minrp;
        }
        #endregion
    }
}
