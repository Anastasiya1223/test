﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Onesky : Snowmobiles
    {
        #region Fields
        /// Вместительность
        private int v;
        #endregion

        #region Propetries
        public int Vmestimoct
        {
            get
            {
                return v;
            }
            set
            {
                if (value >= 1 && value < 4)
                    v = value;
                else
                {
                    v = 2;
                    Console.WriteLine("Неудачная попытка установки значения {0} мест для cнегохода {1}. Установлено значение по умолчанию (2 места).", value, v);
                }
            }
        }
        #endregion

        #region Methods
        public override void Print()
        {
            base.Print();
            Console.WriteLine("МЕст: {0}", v);
        }
        #endregion

        #region Constructors
        public Onesky(int nm, string vd, int yea, int mas, int num, int v) : base(nm, vd, yea, mas, num)
        {
            Vmestimoct = v;
        }
        #endregion
    }
}
