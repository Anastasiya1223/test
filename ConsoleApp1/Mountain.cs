﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Mountain : Twoskying
    {
        #region Fields
        /// максимальная скорость
        private int maxv;
        /// цвет (поле)
        private string color;
        #endregion

        #region Propetries
        public int Maxv
        {
            get
            {
                return maxv;
            }
            set
            {
                if (value >= 50 && value < 220)
                    maxv = value;
                else
                {
                    maxv = 150;
                    Console.WriteLine("Неудачная попытка установки значения {0} км/ч для cнегохода {1}. Установлено значение по умолчанию (150 км/ч).", value, maxv);
                }
            }
        }
        public string Color
        {
            get { return color; }
            set { if (value.Length > 20) Console.WriteLine("Что-то пошло не так. Установлено значение по умолчанию (белый) для снегохода {0}", value); color = "Белый"; }
        }
        #endregion

        #region Methods
        /// сменить цвет
        /// <param name="newcolor">новый цвет</param>
        public void ChangeColor(string newcolor)
        {
            Color = newcolor;
        }
        public override void Print()
        {
            base.Print();
            Console.WriteLine("Максимальная скорость: {0}", maxv);
            Console.WriteLine("Цвет: {0}", Color);
        }
        #endregion

        #region Constructors
        public Mountain(int nm, string vd, int yea, int mas, int num, int minrp, int maxv, string color) : base(nm, vd, yea, mas, num, minrp)
        {
            Maxv = maxv;
            Color = color;
        }
        #endregion
    }
}